default:
	@echo read the makefile

comp: compile
compile:
	tsc --outDir build src/fofa.ts

run:
	node build/fofa.js

ui:
	npm install
	npm start
