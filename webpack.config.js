const HtmlWebpackPlugin = require("html-webpack-plugin");

module.exports = {
  entry: "./src/fofaui.js",
  mode: "development",
  module: {
    rules: [
      {
        test: /\.(js)$/,
        use: "babel-loader",
      },
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: "./src/index.html"
    })
  ]
};
