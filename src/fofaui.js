import React from 'react';
import ReactDOM from 'react-dom';

const container = document.getElementById('root');
ReactDOM.render(<h1>Hello, world!</h1>, container)
